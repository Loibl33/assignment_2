package app.services.front_end_services;

import app.models.front_end_models.Track;
import app.services.helpers.Helpers;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Service
public class TrackSearchService {

    public ArrayList<Track> findTracksByName(String searchString) {
        ArrayList<Track> trackList = new ArrayList<>();

        if (searchString.trim().isEmpty()) {
            return trackList;
        }

        Track track;

        String sqlString = "Select Track, Artist, Album, Genre from  (Select t.Name as Track, A2.Name as Artist, A.Title as Album, G.Name as Genre from Track t join Album A on t.AlbumId = A.AlbumId join Artist A2 on A.ArtistId = A2.ArtistId join Genre G on t.GenreId = G.GenreId) where Track Like '%" + searchString + "%'";
        try {

            ResultSet resultSet = Helpers.ConnectionStatement(sqlString);
            while (resultSet.next()) {

                String name = resultSet.getString("Track");
                String artist = resultSet.getString("Artist");
                String album = resultSet.getString("Album");
                String genre = resultSet.getString("Genre");
                track = new Track(name, artist, album, genre);

                trackList.add(track);

            }
            Helpers.closeConnection();
        } catch (
                SQLException e) {
            e.printStackTrace();
        }

        return trackList;
    }
}
