package app.services.front_end_services;

import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface RandomFeatRepository<T> {
    Collection<T> getFiveRandom();
}
