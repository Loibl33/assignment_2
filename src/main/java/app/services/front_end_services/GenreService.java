package app.services.front_end_services;

import app.models.Genre;
import app.services.helpers.Helpers;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class GenreService implements RandomFeatRepository<Genre> {
    @Override
    public Collection<Genre> getFiveRandom() {
        List<Genre> fiveRandomGenres = new ArrayList<>();

        String sqlString = String.format("Select Name from Genre order by random() limit 5");
        try {
            ResultSet resultSet = Helpers.ConnectionStatement(sqlString);
            while (resultSet.next()) {
                String name = resultSet.getString("Name");

                fiveRandomGenres.add(new Genre(name));
            }

            Helpers.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return fiveRandomGenres;
    }
}
