package app.services.front_end_services;

import app.services.helpers.Helpers;
import org.springframework.stereotype.Service;
import app.models.front_end_models.TrackShort;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class TrackShortService implements RandomFeatRepository<TrackShort> {
    @Override
    public Collection<TrackShort> getFiveRandom() {
        List<TrackShort> fiveRandomTracks = new ArrayList<>();

        String sqlString = String.format("Select Name from Track order by random() limit 5");
        try {

            ResultSet resultSet = Helpers.ConnectionStatement(sqlString);
            while (resultSet.next()) {
                String name = resultSet.getString("Name");

                fiveRandomTracks.add(new TrackShort(name));
            }

            Helpers.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return fiveRandomTracks;
    }
}
