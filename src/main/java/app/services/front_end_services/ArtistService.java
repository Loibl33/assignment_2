package app.services.front_end_services;

import app.models.front_end_models.Artist;
import app.services.helpers.Helpers;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Service
public class ArtistService implements RandomFeatRepository<Artist> {
        @Override
        public Collection<Artist> getFiveRandom() {
            List<Artist> fiveRandomArtist = new ArrayList<>();

            String sqlString = "Select Name from Artist order by random() limit 5";
            try {
                ResultSet resultSet = Helpers.ConnectionStatement(sqlString);

                while (resultSet.next()) {
                    String name = resultSet.getString("Name");

                    fiveRandomArtist.add(new Artist(name));
                }

                Helpers.closeConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return fiveRandomArtist;
        }
    }
