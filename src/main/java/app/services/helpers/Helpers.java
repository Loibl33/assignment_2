package app.services.helpers;

import app.models.back_end_models.CustomerShort;
import app.models.back_end_models.CustomerSpender;
import app.services.DatabaseConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Helpers {
    private static Connection conn;


    public static ResultSet ConnectionStatement(String query) throws SQLException {
        conn = DatabaseConnectionManager.getConnection();
        return conn.prepareStatement(query).executeQuery();
    }

    public static ResultSet ConnectionStatement(String query, int... args) throws SQLException {
        conn = DatabaseConnectionManager.getConnection();
        PreparedStatement st = conn.prepareStatement(query);

        for (int i = 0; i < args.length; i++) {
            st.setInt(i + 1, args[i]);
        }

        return st.executeQuery();
    }

    public static ResultSet ConnectionStatement(String query, String... args) throws SQLException {
        conn = DatabaseConnectionManager.getConnection();
        PreparedStatement st = conn.prepareStatement(query);

        for (int i = 0; i < args.length; i++) {
            st.setString(i, args[i]);
        }

        return st.executeQuery();
    }

    public static void closeConnection() throws SQLException {
        conn.close();
    }

    public static boolean connectionStatementExecute(String query) throws SQLException {
        conn = DatabaseConnectionManager.getConnection();
        return conn.prepareStatement(query).execute();
    }

    public static List<CustomerShort> createShortCustomer(String sqlString) {
        CustomerShort customer;
        ArrayList<CustomerShort> customers = new ArrayList<>();
        try {
            ResultSet resultSet = Helpers.ConnectionStatement(sqlString);
            while (resultSet.next()) {

                customer = getCustomerShort(resultSet);
                customers.add(customer);
            }

            Helpers.closeConnection();
        } catch (
                SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }

    public static CustomerShort creteShortCustomer(String sqlString) {
        CustomerShort customer = null;
        try {
            ResultSet resultSet = Helpers.ConnectionStatement(sqlString);

            customer = getCustomerShort(resultSet);

            Helpers.closeConnection();

        } catch (
                SQLException e) {
            e.printStackTrace();
        }

        return customer;
    }

    public static CustomerSpender createCustomerSpender(String sqlString){
        CustomerSpender spenders = new CustomerSpender();

        try {
            ResultSet resultSet = Helpers.ConnectionStatement(sqlString);
            while (resultSet.next()) {
                CustomerShort customer = getCustomerShort(resultSet);
                double totalSpend = resultSet.getDouble("SumPerCustomer");
                spenders.put(customer,totalSpend);
            }

            Helpers.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        return spenders;
    }

    private static CustomerShort getCustomerShort(ResultSet resultSet) throws SQLException {
        String id = resultSet.getString("CustomerId");
        String firstName = resultSet.getString("FirstName");
        String lastName = resultSet.getString("LastName");
        String country = resultSet.getString("Country");
        String postalCode = resultSet.getString("PostalCode");
        String phone = resultSet.getString("Phone");
        String email = resultSet.getString("Email");

        return new CustomerShort(id, firstName, lastName, country, postalCode, phone, email){
            @Override
            public String toString() {
                return String.format("%s %s", firstName, lastName);
            }
        };
    }
}
