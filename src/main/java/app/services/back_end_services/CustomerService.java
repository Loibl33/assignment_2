package app.services.back_end_services;

import app.models.back_end_models.CustomerShort;
import app.services.helpers.Helpers;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class CustomerService {

    public CustomerShort getCustomerById(String id) {
        String sqlString = String.format("Select * from Customer where CustomerId = %s", id);
        return Helpers.creteShortCustomer(sqlString);
    }

    public List<CustomerShort> getAllCustomers() {
        String sqlString = "Select CustomerId, FirstName, Lastname, Country, PostalCode, Phone, Email from Customer";
        return Helpers.createShortCustomer(sqlString);
    }

    public List<CustomerShort> findCustomersByName(String name) {
        String sqlString = "Select * from Customer where LastName like '%" + name + "%' or FirstName like '%" + name + "%'";
        return Helpers.createShortCustomer(sqlString);
    }

    public List<CustomerShort> getPageOfCustomers(String limit, String offset) {
        int startId = (Integer.parseInt(offset) - 1) * Integer.parseInt(limit);
        int endId = startId + Integer.parseInt(limit);
        String sqlString = "select * from Customer where CustomerId >= " + startId + " and CustomerId < " + endId;

        return Helpers.createShortCustomer(sqlString);
    }

    public boolean createCustomer(CustomerShort customer) {

        String firstname = customer.getFirstName();
        String lastname = customer.getLastName();
        String country = customer.getCountry();
        String postalCode = customer.getPostalCode();
        String phone = customer.getPhone();
        String mail = customer.getEmail();

        boolean isSuccessful = false;

        String sqlString = String.format("insert into Customer( FirstName, LastName, Country, PostalCode, Phone,Email) values ('%s', '%s', '%s', '%s', '%s', '%s')", firstname, lastname, country, postalCode, phone, mail);

        try {
            Helpers.connectionStatementExecute(sqlString);
            isSuccessful = true;
            Helpers.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return isSuccessful;
    }

    public boolean updateCustomerById(String id, CustomerShort customer) {
        boolean successful = false;
        String sqlString = String.format("update Customer set FirstName='%s', LastName='%s', Country='%s', PostalCode='%s', Phone='%s',Email='%s' where CustomerId='%s'",
                customer.getFirstName(),
                customer.getLastName(),
                customer.getCountry(),
                customer.getPostalCode(),
                customer.getPhone(),
                customer.getEmail(),
                id);

        try {
            Helpers.connectionStatementExecute(sqlString);
            successful = true;
            Helpers.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return successful;
    }
}



