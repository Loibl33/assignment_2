package app.services.back_end_services;

import app.models.back_end_models.CustomerCountry;
import app.services.helpers.Helpers;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class CustomerCountryService {
    public CustomerCountry getCustomerCountries() {
        CustomerCountry cc = new CustomerCountry();

        int count;
        String country;
        String sqlString = "SELECT COUNT(CustomerId) as CustomerCount, Country FROM Customer GROUP BY Country ORDER BY COUNT(CustomerId) DESC";

        try {
            ResultSet resultSet = Helpers.ConnectionStatement(sqlString);

            while (resultSet.next()) {
                country = resultSet.getString("Country");
                count = resultSet.getInt("CustomerCount");

                cc.put(country, count);
            }

            Helpers.closeConnection();
        } catch (
                SQLException e) {
            e.printStackTrace();
        }

        return cc;
    }


}
