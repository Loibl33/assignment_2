package app.services.back_end_services;

import app.models.Genre;
import app.services.helpers.Helpers;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class CustomerGenreService extends HashMap<CustomerService, String> {
    public List<Genre> findFavouriteGenre(String customerId) {
        List<Genre> popularGenres = new ArrayList<>();
        String sqlString = "Select CustomerId, FirstName, LastName,Country,  City, PostalCode, Phone, Email, max(GenreCount) as MaxGenreCount, GenreName from ( Select  c.*, count(G.Name) as GenreCount, G.Name as GenreName from Customer c join Invoice I on c.CustomerId = I.CustomerId join InvoiceLine  on I.InvoiceId = InvoiceLine.InvoiceId join Track T on InvoiceLine.TrackId = T.TrackId join Genre G on T.GenreId = G.GenreId  group by c.CustomerId,g.Name) where CustomerId = " + customerId + "  group by CustomerId";

        try {
            ResultSet resultSet = Helpers.ConnectionStatement(sqlString);

            while (resultSet.next()) {
                String genreName = resultSet.getString("GenreName");

                popularGenres.add(new Genre(genreName));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return popularGenres;
    }
}
