package app.services.back_end_services;

import app.models.back_end_models.CustomerSpender;
import app.services.DatabaseConnectionManager;
import app.services.helpers.Helpers;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerSpenderService {
    public CustomerSpender findHighestSpenders() {
        String sqlString = "Select * from customer c join (Select CustomerId, Round(SUM(Total), 2) as SumPerCustomer  from Invoice GROUP BY CustomerId ) i on c.CustomerId = i.CustomerId order by SumPerCustomer desc limit 10";
        return Helpers.createCustomerSpender(sqlString);
    }
}
