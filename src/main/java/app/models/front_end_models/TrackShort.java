package app.models.front_end_models;

public class TrackShort {
    private String name;

    public TrackShort(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
