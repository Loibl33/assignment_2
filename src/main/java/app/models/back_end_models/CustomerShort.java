package app.models.back_end_models;

public class CustomerShort {
    private String id;
    private String firstName;
    private String lastName;
    private String country;
    private String postalCode;
    private String Phone;
    private String email;

    public CustomerShort(String id, String firstName, String lastName, String country, String postalCode, String phone, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.postalCode = postalCode;
        Phone = phone;
        this.email = email;
    }
    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCountry() {
        return country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getPhone() {
        return Phone;
    }

    public String getEmail() {
        return email;
    }
}
