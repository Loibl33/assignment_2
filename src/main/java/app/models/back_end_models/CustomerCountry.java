package app.models.back_end_models;

import java.util.LinkedHashMap;

public class CustomerCountry extends LinkedHashMap<String, Integer> {
    private static int instanceCount = 0;
    public CustomerCountry() {
        instanceCount++;
    }

    public static int getInstanceCount() {
        return instanceCount;
    }
}
