package app.controllers.front_end_controllers;

import app.models.front_end_models.Track;
import app.services.front_end_services.TrackSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@Controller
public class SearchResultsController {
    private final TrackSearchService trackSearch;

    public SearchResultsController(@Autowired TrackSearchService trackSearch) {
        this.trackSearch = trackSearch;
    }

    @GetMapping("/results")
    public String searchPage(@RequestParam("search") String terms, Model model) {
        ArrayList<Track> tracksByName = trackSearch.findTracksByName(terms);
        model.addAttribute("tracks", tracksByName);

        return "searchResPage";
    }
}
