package app.controllers.front_end_controllers;

import app.models.front_end_models.Artist;
import app.models.Genre;
import app.models.front_end_models.TrackShort;
import app.services.front_end_services.RandomFeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@Controller
public class PageController {
    private final RandomFeatRepository<Artist> artistRepository;
    private final RandomFeatRepository<Genre> genreRepository;
    private final RandomFeatRepository<TrackShort> rackShortRepository;

    public PageController(@Autowired RandomFeatRepository<Artist> artistRepository,
                          @Autowired RandomFeatRepository<Genre> genreRepository,
                          @Autowired RandomFeatRepository<TrackShort> rackShortRepository) {
        this.artistRepository = artistRepository;
        this.genreRepository = genreRepository;
        this.rackShortRepository = rackShortRepository;
    }

    @GetMapping("/")
    public String homePage(Model model) {
        Collection<Artist> fiveRandomArtists = this.artistRepository.getFiveRandom();
        Collection<Genre> fiveRandomGenres = this.genreRepository.getFiveRandom();
        Collection<TrackShort> fiveRandomTrack = this.rackShortRepository.getFiveRandom();

        model.addAttribute("artists", fiveRandomArtists);
        model.addAttribute("genres", fiveRandomGenres);
        model.addAttribute("tracks", fiveRandomTrack);
        return "index";
    }
}

