package app.controllers.rest_controllers;

import app.models.Genre;
import app.models.back_end_models.CustomerCountry;
import app.models.back_end_models.CustomerShort;
import app.models.back_end_models.CustomerSpender;
import app.services.back_end_services.CustomerCountryService;
import app.services.back_end_services.CustomerGenreService;
import app.services.back_end_services.CustomerService;
import app.services.back_end_services.CustomerSpenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api")
public class CustomerController {
    private final CustomerService customerService;
    private final CustomerCountryService customerCountryService;
    private final CustomerSpenderService customerSpenderService;
    private final CustomerGenreService customerGenreService;

    public CustomerController(@Autowired CustomerService customerService,
                              @Autowired CustomerCountryService customerCountryService,
                              @Autowired CustomerSpenderService customerSpenderService,
                              @Autowired CustomerGenreService customerGenreService) {
        this.customerService = customerService;
        this.customerCountryService = customerCountryService;
        this.customerSpenderService = customerSpenderService;
        this.customerGenreService = customerGenreService;
    }


    @GetMapping("/customers/all")
    public List<CustomerShort> getAllCustomers() {
        return this.customerService.getAllCustomers();
    }

    @GetMapping("/customers/by_id")
    public CustomerShort getCustomerById(@RequestParam("where_id") String id) {
        return this.customerService.getCustomerById(id);
    }

    @GetMapping("/customers/by_name")
    public List<CustomerShort> getCustomerByName(@RequestParam("where_name") String name) {
        return this.customerService.findCustomersByName(name);
    }

    @GetMapping("/customers")
    //http://localhost:8081/api/customers?offset=1&limit=10
    public List<CustomerShort> getPageOfCustomers(@RequestParam("offset") String offset, @RequestParam("limit") String limit) {
        return this.customerService.getPageOfCustomers(limit, offset);
    }

    @PostMapping("/customers/create")
    public boolean createCustomer(@RequestBody CustomerShort customer) {
        boolean isCreated = this.customerService.createCustomer(customer);
        System.out.println(isCreated);
        return isCreated;
    }

    @PutMapping("customers/update/{id}")
    public boolean updateCustomer(@PathVariable String id, @RequestBody CustomerShort customer) {
        return this.customerService.updateCustomerById(id, customer);
    }

    @GetMapping("customers/group_by_country")
    public CustomerCountry getCustomersGroupedByCountry() {
        return this.customerCountryService.getCustomerCountries();
    }

    @GetMapping("customers/total_spend")
    public CustomerSpender getTotalSpenders() {
        return this.customerSpenderService.findHighestSpenders();
    }

    @GetMapping("customers/most_popular_genre")
    public List<Genre> getMostPopularGenre(@RequestParam("where_id") String id) {
        return this.customerGenreService.findFavouriteGenre(id);
    }
}
