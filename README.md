# Assignment-2: iTunes
This application is a hybrid app. It has two front-end pages and also a little REST Service with JSON Response.
## Configuration:
You are welcome to clone the repo!
The application is built on Maven  v.4.0.0 with Java v. 8 and Thymeleaf for the front-end.
If you want to run the application locally, you have 2 options.
- 1
    - You must change the server.port variable in src>main>resources>application.properties with the port you would like to use, and then simply start the Main method.
- 2
    - You can build a Docker image and use it to run the application.
      <br>
### Usage
You can access the front-end functionality or the REST Service of the application using the following link:
- Front-end functionality:  https://assignment-noroff-2.herokuapp.com/
    - Homepage shows the 5 random Artists, 5 random Tracks, 5 random Genres
    - Homepage also has search functionality. You can search for a song by the song's name.
- REST Service: You can make API calls on https://assignment-noroff-2.herokuapp.com/api using the following routes:
    - /customers/all -> returns all customers : GET Request
    - /customers/by_id?where_id={int id} -> returns customer by id : GET Request
    - /customers/by_id?where_name={string name} -> returns customer by name : GET Request
    - /customers?offset={number}&limit={number} -> returns a page of customers : GET Request
    - /customers/create : POST Request with Header "Content-type":"application/json" and a body in JSON format with the following properties:
        - {
          "firstName": "Test",
          "lastName": "Tester",
          "country": "Test",
          "postalCode": "Test",
          "phone": "Test",
          "email": "test@gmail.com"
          }
    - /customers/update/{int id} : PUT Request with Header "Content-type":"application/json" and a body in JSON format with the following properties:
        - {
          "firstName": "Test-updated",
          "lastName": "Tester-updated",
          "country": "Test-updated",
          "postalCode": "Test-updated",
          "phone": "Test-updated",
          "email": "test@gmail.com-updated"
          }
    - /customers/group_by_country - Returns all countries with number of customers related to each country: GET Request
    - /customers/total_spend - Returns 10 customers who spend the most money sorted in desc order: GET Request
    - /customers/most_popular_genre?where_id={int id} Returns the favorite genres of one customer specified by id: GET Request
    
  
You can also check this postman collection. It contains all API calls that are listed above. [![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/166c4b72d36eb2999d23?action=collection%2Fimport)


## Frond-End/UI
The UI of the application looks like this:
Homepage :

![WhatsApp Image 2022-02-24 at 16 00 16](https://user-images.githubusercontent.com/79804094/155557730-3a0df262-e313-4357-9dea-4622b53660b5.jpeg)

Results page:

![WhatsApp Image 2022-02-24 at 15 59 59](https://user-images.githubusercontent.com/79804094/155558090-ae6dd744-b9f7-4479-962c-7d9c22f32cd5.jpeg)

![WhatsApp Image 2022-02-24 at 16 00 30](https://user-images.githubusercontent.com/79804094/155558263-7789b9a9-a6ef-4f26-9dc3-41dc70b425ff.jpeg)

## Maintainer
[Petar Dimitrov]

[Philipp Loibl]
## License
[MIT]
---
[Petar Dimitrov]: https://github.com/PetarDimitrov91
[Philipp Loibl]: https://github.com/Loibl33
[MIT]: https://choosealicense.com/licenses/mit/